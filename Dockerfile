FROM centos:7

RUN yum install -y python3 python3-pip

RUN pip3 install flask flask-jsonpify flask-restful

COPY python-api.py /python-api/python-api.py

CMD ["python3", "/python-api/python-api.py"]
